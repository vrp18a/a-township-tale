﻿using System;
using UnityEngine;
using UnityEngine.VR;
using System.Collections.Generic;
using Accord.Math;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;
using Accord.Statistics.Analysis;
using Accord.Statistics.Models.Regression.Linear;
using Accord.MachineLearning.VectorMachines;
using System.IO;
using System.Globalization;


public class Classifier : MonoBehaviour
{
    private static List<UnityEngine.Vector3> leftPositions = new List<UnityEngine.Vector3>();
    private static List<UnityEngine.Vector3> rightPositions = new List<UnityEngine.Vector3>();
    private static List<Quaternion> leftRotations = new List<Quaternion>();
    private static List<Quaternion> rightRotations = new List<Quaternion>();
    private static List<UnityEngine.Vector3> leftVelocitys = new List<UnityEngine.Vector3>();
    private static List<UnityEngine.Vector3> rightVelocitys = new List<UnityEngine.Vector3>();
    private static List<UnityEngine.Vector3> headPostions = new List<UnityEngine.Vector3>();
    private static List<Quaternion> headRotations = new List<Quaternion>();
    public static Boolean record = false;
    private static int frame = 0;
    private static PrincipalComponentAnalysis pca = null;
    private static MulticlassSupportVectorMachine<Linear> svm = null;
    private static int windowLength = 100;


    private void Start()
    {

        double[][] originalInputs;
        int[] originalOutputs;
        int[] outputs;
        double[][] inputs;

        //Load the training data from txt file and convert it to the format that PCA needs
        LoadTrainData(out originalInputs, out originalOutputs);
        ConvertData(originalInputs, originalOutputs, out outputs, out inputs);

        // Create the Multi-label learning algorithm for the machine
        var teacher = new MulticlassSupportVectorLearning<Linear>()
        {
            Learner = (p) => new SequentialMinimalOptimization<Linear>()
            {
                Complexity = 10000.0 // Create a hard SVM
            }
        };

        //Select the feature of data by applying PCA
        pca = PCA(inputs);
        var trainDataFromPCA = pca.Transform(inputs);

        // Teach the vector machine
        svm = teacher.Learn(trainDataFromPCA, outputs);

    }

    private void Update()
    {

        //Read the position 
        UnityEngine.Vector3 positionL = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        UnityEngine.Vector3 positionR = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

        //Read the rotation 
        Quaternion rotationL = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
        Quaternion rotationR = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);

        //Read the velocity
        UnityEngine.Vector3 velocityL = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
        UnityEngine.Vector3 velocityR = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);

        //Read the data from headset
        UnityEngine.Vector3 positionHead = InputTracking.GetLocalPosition(VRNode.Head);
        Quaternion rotationHead = InputTracking.GetLocalRotation(VRNode.Head);

        //Store everything into the List<Vector3>
        leftPositions.Add(positionL);
        rightPositions.Add(positionR);
        leftRotations.Add(rotationL);
        rightRotations.Add(rotationR);
        leftVelocitys.Add(velocityL);
        rightVelocitys.Add(velocityR);
        headPostions.Add(positionHead);
        headRotations.Add(rotationHead);
        frame++;


        if ((frame + 1) > windowLength)
        {
            Classify();
            ClearMemory();
        }
    }

    public static void Classify()
    {
        List<double> list = new List<double>();
        for (int i = 0; i < windowLength; i++) {
            list.Add(leftPositions[i][0]);
            list.Add(leftPositions[i][1]);
            list.Add(leftPositions[i][2]);

            list.Add(leftVelocitys[i][0]);
            list.Add(leftVelocitys[i][1]);
            list.Add(leftVelocitys[i][2]);

            list.Add(leftRotations[i][0]);
            list.Add(leftRotations[i][1]);
            list.Add(leftRotations[i][2]);
            list.Add(leftRotations[i][3]);

            list.Add(rightPositions[i][0]);
            list.Add(rightPositions[i][1]);
            list.Add(rightPositions[i][2]);

            list.Add(rightVelocitys[i][0]);
            list.Add(rightVelocitys[i][1]);
            list.Add(rightVelocitys[i][2]);

            list.Add(rightRotations[i][0]);
            list.Add(rightRotations[i][1]);
            list.Add(rightRotations[i][2]);
            list.Add(rightRotations[i][3]);

            list.Add(headPostions[i][0]);
            list.Add(headPostions[i][1]);
            list.Add(headPostions[i][2]);

            list.Add(headRotations[i][0]);
            list.Add(headRotations[i][1]);
            list.Add(headRotations[i][2]);
            list.Add(headRotations[i][3]);
        }
        double[][] inputs = new double[1][];
        inputs[0] = list.ToArray();
        list.Clear();
        var testDataFromPCA = pca.Transform(inputs);

        int[] answers = svm.Decide(testDataFromPCA);

        if (answers[0] == 0)
        {
            print("Öutput is No behavior");
        }
        else if (answers[0] == 1)
        {
            print("Öutput is Waving");
        }
        else if (answers[0] == 2)
        {
            print("Öutput is Punching");
        }
        else if (answers[0] == 3)
        {
            print("Öutput is Shaking hands");
        }
        else if (answers[0] == 4)
        {
            print("Öutput is Swing a sword!!!");
        }
        else if (answers[0] == 5)
        {
            print("Öutput is No(ShakeHead)");
        }
    }

    public static void ClearMemory()
    {
        //Clear the memory used
        leftPositions.Clear();
        rightPositions.Clear();
        leftRotations.Clear();
        rightRotations.Clear();
        leftVelocitys.Clear();
        rightVelocitys.Clear();
        headPostions.Clear();
        headRotations.Clear();
        frame = 0;
    }
    

    //Converting the row data to the format that can be applied to PCA
    //Group each 50 frames together and store them in one 1D array, and consider this array as one behavior data we are going to use
    //The expected ouput for each behavior is decided the largest number of classes within the 50 frames
    //Currently, I just put two types of behavior (waving -> 1 and punching -> 2) plus all the other data considered to be "other behaviors" -> 0 here
    //Feel free to add more types of behaviors by adding more class counters and each counter represents the number of frames that is from the specific 
    //behavior data set.
    public static void ConvertData(double[][] rawData, int[] rawData2, out int[] outputs, out double[][] inputs)
    {
        var originalInputs = rawData;
        int total = originalInputs.GetTotalLength();
        List<List<double>> list = new List<List<double>>();
        List<double> tmp = new List<double>();
        List<int> tmp2 = new List<int>();
        //Identify the counters of frames for the behaviors
        //Iterate through the frames and add each attribute to a list and convert the list to an array at the end
        int one = 0, zero = 0, two = 0, three = 0, four = 0, five = 0;
        for (int i = 0; i < rawData.GetLength(0); i++)
        {
            for (int j = 0; j < rawData[0].Length; j++)
            {
                tmp.Add(rawData[i][j]);
            }
            if (rawData2[i] == 1)
            {
                one++;
            }
            else if (rawData2[i] == 0)
            {
                zero++;
            }
            else if (rawData2[i] == 2)
            {
                two++;
            }
            else if (rawData2[i] == 3)
            {
                three++;
            }
            else if (rawData2[i] == 4)
            {
                four++;
            }
            else if (rawData2[i] == 5)
            {
                five++;
            }
            if ((i + 1) % windowLength == 0)
            {
                list.Add(new List<double>(tmp));
                tmp.Clear();
                int val = 0, max = zero;
                if (one > max)
                {
                    max = one;
                    val = 1;
                }
                if (two > max)
                {
                    max = two;
                    val = 2;
                }
                if (three > max)
                {
                    max = three;
                    val = 3;
                }
                if (four > max)
                {
                    max = four;
                    val = 4;
                }
                if (five > max)
                {
                    max = five;
                    val = 5;
                }
                //add the expect the output to the output array
                tmp2.Add(val);

                //clear the counters
                one = 0;
                zero = 0;
                two = 0;
                three = 0;
                four = 0;
                five = 0;
            }
        }
        outputs = tmp2.ToArray();
        inputs = new double[list.Count][];
        for (int i = 0; i < list.Count; i++)
        {
            inputs[i] = list[i].ToArray();
        }
    }

    public static void LoadTrainData(out double[][] trainData, out int[] trainClass)
    {
        StreamReader reader = new StreamReader("D:/COMP3615/OurData/trainingData.txt");
        string line;
        List<List<double>> list = new List<List<double>>();
        List<double> tmp = new List<double>();
        List<int> tmp2 = new List<int>();

        // read the data line by line and store in a list<list<double>>
        while ((line = reader.ReadLine()) != null)
        {
            string[] data = line.Split();
            for (int i = 0; i < data.Length - 1; i++)
            {
                double temp = Convert.ToDouble(data[i], CultureInfo.InvariantCulture);
                tmp.Add(temp);
            }
            tmp2.Add(Convert.ToInt32(data[data.Length - 1]));
            list.Add(new List<double>(tmp));
            tmp.Clear();
        }

        trainData = new double[list.Count][];
        trainClass = tmp2.ToArray();

        for (int i = 0; i < list.Count; i++)
        {
            trainData[i] = list[i].ToArray();
        }
        reader.Close();
    }

    public static PrincipalComponentAnalysis PCA(double[][] input)
    {
        double[][] data = input;

        // Let's create an analysis with centering (covariance method)
        // but no standardization (correlation method) and whitening:
        var pca = new PrincipalComponentAnalysis()
        {
            Method = PrincipalComponentMethod.Center,
            Whiten = true
        };

        // Now we can learn the linear projection from the data
        MultivariateLinearRegression transform = pca.Learn(data);

        // Finally, we can project all the data
        //double[][] output1 = pca.Transform(data);

        // Or just its first components by setting 
        // NumberOfOutputs to the desired components:
        // pca.NumberOfOutputs = 1;

        // And then calling transform again:
        //double[][] output2 = pca.Transform(data);

        // We can also limit to 90% of explained variance:
        pca.ExplainedVariance = 0.9;

        // And then call transform again:
        // double[][] output3 = pca.Transform(data);

        return pca;
    }
}