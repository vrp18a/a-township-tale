using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.UI;
using System.Collections.Generic;

public class DataRecorder : MonoBehaviour
{
    private static List<Vector3> leftPositions = new List<Vector3>();
    private static List<Vector3> rightPositions = new List<Vector3>();
    private static List<Quaternion> leftRotations = new List<Quaternion>();
    private static List<Quaternion> rightRotations = new List<Quaternion>();
    private static List<Vector3> leftVelocitys = new List<Vector3>();
    private static List<Vector3> rightVelocitys = new List<Vector3>();
    private static List<Vector3> headPostions = new List<Vector3>();
    private static List<Quaternion> headRotations = new List<Quaternion>();
    public static StreamWriter writer;
	public static Boolean record = false;
    private static int frame = 0;
    private static long totalFrame = 0;

    private void Start()
    {
        GameObject.Find("StopButton").GetComponent<Button>().interactable = false;
        GameObject.Find("PathInput").GetComponent<InputField>().text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).Replace('\\','/');
        GameObject.Find("FilenameInput").GetComponent<InputField>().text = "untitled.txt";
    }
	
    private void Update()
    {
        //print("false");
        if (!record)
			return;

        //print("true");
		//Read the data from controllers
        
        //Read the position 
        Vector3 positionL = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        Vector3 positionR = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
        //Read the rotation 
        Quaternion rotationL = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
        Quaternion rotationR = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);
        //Read the velocity
        Vector3 velocityL = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
        Vector3 velocityR = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
        //Read the data from head
        Vector3 positionHead = InputTracking.GetLocalPosition(VRNode.Head);
        Quaternion rotationHead = InputTracking.GetLocalRotation(VRNode.Head);
        //Store everything into the List<Vector3>
        leftPositions.Add(positionL);
        rightPositions.Add(positionR);
        leftRotations.Add(rotationL);
        rightRotations.Add(rotationR);
        leftVelocitys.Add(velocityL);
        rightVelocitys.Add(velocityR);
        headPostions.Add(positionHead);
        headRotations.Add(rotationHead);
        //Increment the frameCount, and write the data to the file and clear the lists for every 1000 frames
        frame++;
        
        
        if (frame > 1000)
        {
            WriteToFile();
            ClearMemory();
        }
    }

    public static void ClearMemory()
    {
        //Clear the memory used
        leftPositions.Clear();
        rightPositions.Clear();
        leftRotations.Clear();
        rightRotations.Clear();
        leftVelocitys.Clear();
        rightVelocitys.Clear();
        headPostions.Clear();
        headRotations.Clear();
        frame = 0;
    }

    public static void WriteToFile()
    {
        frame = 0;
        //Write the data to the file, everything in one line for each frame
        for (int i = 0; i < leftPositions.Count; i++)
        {
            totalFrame++;
            writer.WriteLine(
                leftPositions[i].ToString("F4") + "," + 
                leftVelocitys[i].ToString("F4") + "," + 
                leftRotations[i].ToString("F4") + "," +
                rightPositions[i].ToString("F4") + "," +
                rightVelocitys[i].ToString("F4") + "," +
                rightRotations[i].ToString("F4") + "," +
                headPostions[i].ToString("F4") + "," +
                headRotations[i].ToString("F4") + "," +
                totalFrame.ToString());
        }
    }

    private void OnDestroy()
    {
        WriteToFile();
        writer.Close();
    }
}