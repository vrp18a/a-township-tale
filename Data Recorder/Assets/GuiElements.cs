﻿using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GuiElements : MonoBehaviour
{
    //private InputField pathInput;
    //private Button stopButton;
    //private Button recordButton;

    public void BrowseButtonOnClick()
    {
        InputField pathInput = GameObject.Find("PathInput").GetComponent<InputField>();
        pathInput.text = EditorUtility.OpenFolderPanel("Select folder", "", "");
    }

    public void RecordButtonOnClick()
    {
        InputField pathInput = GameObject.Find("PathInput").GetComponent<InputField>();
        InputField filenameInput = GameObject.Find("FilenameInput").GetComponent<InputField>();

        if (pathInput.text == "" || filenameInput.text == "")
        {
            GameObject.Find("WarnText").GetComponent<Text>().text = "Path and file name cannot be empty!";
            return;
        }

        if (File.Exists(filenameInput.text))
        {
            DataRecorder.writer = File.AppendText(pathInput.text + "\\" + filenameInput.text);
            DataRecorder.writer.WriteLine("RecordStart");
            DataRecorder.writer.WriteLine("LeftControllerPosition, LeftControllerVelocity, LeftControllerRotation, RightControllerPosition, RightControllerVelocity, RightControllerRotation, HeadPosition, HeadVelocity, frame");
        }
        else
        {
            DataRecorder.writer = File.CreateText(pathInput.text + "\\" + filenameInput.text);
            DataRecorder.writer.WriteLine("RecordStart");
            DataRecorder.writer.WriteLine("LeftControllerPosition, LeftControllerVelocity, LeftControllerRotation, RightControllerPosition, RightControllerVelocity, RightControllerRotation, HeadPosition, HeadVelocity, frame");
        }

        DataRecorder.record = true;

        pathInput.interactable = false;
        filenameInput.interactable = false;
        GameObject.Find("RecordButton").GetComponent<Button>().interactable = false;
        GameObject.Find("StopButton").GetComponent<Button>().interactable = true;
    }

    public void StopButtonOnClick()
    {
        DataRecorder.record = false;
        DataRecorder.WriteToFile();
        DataRecorder.ClearMemory();

        GameObject.Find("StopButton").GetComponent<Button>().interactable = false;
        GameObject.Find("RecordButton").GetComponent<Button>().interactable = true;
        GameObject.Find("FilenameInput").GetComponent<InputField>().interactable = true;
        GameObject.Find("PathInput").GetComponent<InputField>().interactable = true;
    }
}
