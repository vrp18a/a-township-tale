# VR Behaviour Recognition #

## [Wiki Contents](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/) ##

* [Assessment Outline](https://bitbucket.org/vrp18a/a-township-tale/wiki/Home)
* [Meeting Documents](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Meetings)
	* [Meeting 1](https://bitbucket.org/vrp18a/a-township-tale/wiki/Meetings/Meeting%201%20Summary)
	* [Meeting 2](https://bitbucket.org/vrp18a/a-township-tale/wiki/Meetings/Meeting%202%20Summary)
* [Group Reports](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Reports%20)
	* [Group Report 1](https://docs.google.com/document/d/1JFD9foDvlb6VnJHZ8eQ6YjO3170nXbpFdtZ--zZJkak/edit)
	* [Final Group Report](https://docs.google.com/document/d/1Qd_eB12uxtpruGXSdaLkjxEYFRq-YOJN5rit-WnOvTc/edit#)
* [Individual Reports](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Individual%20Wiki)
	* [Jamie](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Individual%20Wiki/Jamie)
	* [Jeffery](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Individual%20Wiki/Jeffrey)
	* [David](https://drive.google.com/file/d/0B-5VYz7WWZ02TmhtYTVCMUx5S0U/view?usp=sharing)
	* [Ricardo](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Individual%20Wiki/Zhizhen%20Zhou)
	* [Jiaren](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Individual%20Wiki/JiarenYe)
	* [Yunze](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Individual%20Wiki/Yunze%20Qiu)
* [Resources](https://bitbucket.org/vrp18a/a-township-tale/wiki/browse/Resources)
* [First Client Deployment Checklist](https://bitbucket.org/vrp18a/a-township-tale/downloads/Comp3615%20Client%20First%20Deployment%20Altar%20-joel_altavr.io.pdf)
	
## Introduction to the Project ##

The project our group has been assigned to is Behaviour Recognition in VR using Machine Learning. This project was offered to us by Alta, a recent start up company. The project incorporates the use of low to high level machine learning to develop a system that identifies player behaviour in their new VR game "A Township Tale".

The purpose of the game is to design a program that can recognise a series of actions done by a character within the game and through those actions, recognise what the player is feeling at the time and make necessary changes to their character within the game. 

The applicational use Alta has with our product is to change the emotional states of their characters and to use the data we record for analytical use to further improve their game. 
Currently, as their game is still at their early stages, their characters are stoic in both their facial expressions and their other emotional actions. Through the integration of our program, Alta wants to be able to set different character expressions based on their actions, this will be achieved through Machine Learning which encompasses the data we will record. An example of what they are expecting is as such

* Waving: When a character waves, we have recognised this as a friendly gesture and wish to change the character's facial expression from a stoic one to a smile. So the trigger for this will be, when the character within the game moves their open hand in a certain way our program will recognise this action and change the character's facial expression.

* Shadow Boxing: If a character is punching the air in front of another character, we have recognised this as an aggressive action and that it means the character wants to have a fight. What we wish to do with this knowledge is to change the character's expression to one of aggression as well as change their facial colour to red and add steam around it.

### Things to note: ###

* The client does not use the Unity input manager (keyboards, x-box controller, etc.). Client only uses Oculus input. For now, head gear position/rotation (also the Oculus hand controller) is enough but it may be necessary to normalize the data. Client expects the coordinates to have the head of the user as the center coordinates.

* Recommendation by client: Make a playback machine to put in data, press certain key inputs to insert data label (because getting direct data output from the Unity may be somewhat ambiguous).

* General Timeline for a VR game (11 ms per frame), running at 45 fps. Think about normalizing the output data in the future (not feasible to finish by this week, but should be finished by 2 weeks time).